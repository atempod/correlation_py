#!/usr/bin/python --3.4
# -*- coding: utf-8 -*-

# Analise only not negative values:
MYSQL_SELECT = """
        SELECT {field1}, {field2}
        FROM notification_event_archive_002_stats
        WHERE (({field1} IS NOT NULL) AND ({field1} >= 0) AND
               ({field2} IS NOT NULL) AND ({field2} >= 0))
        """

# Analise only positive values:
# MYSQL_SELECT = """
#         SELECT {field1}, {field2}
#         FROM notification_event_archive_002_stats
#         WHERE (({field1} IS NOT NULL) AND ({field1} > 0) AND
#                ({field2} IS NOT NULL) AND ({field2} > 0))
#         """

