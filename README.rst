
The application serves to calculate a correlation coefficient and other statistical parameters for data, collected in database tables.


This application can be installed with Python ver.2.7 to ver.3.5 (with appropriate module versions).

Project Setup
    1. Clone the repository and set it as the current working directory.
    2. (Optional practice) Create a virtual environment.
    3. Loads required libraries into the virtual environment:
        pip install -r requirements-dev.txt
    4. Install numpy:
        pip install numpy
    5. If necessary (if some problems occur), upgrade pip, setuptools and virtualenv (and then repeat numpy install):
        python -m pip install --upgrade pip
        pip install -U setuptools
        pip install -U virtualenv
    6. Install MySQLdb (Python-MySQL driver):
        On Ubuntu:
        sudo apt-get install python-mysqldb
        On Windows:
        install from the file. It's necessary to download correct file for the appropriate operational system and Python version from http://www.lfd.uci.edu/~gohlke/pythonlibs/#mysqlclient. For example, install from file:
        pip install mysqlclient-1.3.7-cp34-none-win_amd64.whl


Run application from command line:
    python correlation.py


Setup SQL queries if necessary:
    SQL query is located in the config.py file.
    The query is written in the form, fit for SQL query from Python.
    One can edit the SQL query by correcting the text of the query.



