#!/usr/bin/python --3.4
# -*- coding: utf-8 -*-
import sys
import MySQLdb
from MySQLdb import Error
import numpy
import statistics
import datetime

from config import MYSQL_SELECT



def benchmark(func):
    """ Decorator for calculation of function operation time  """
    import time
    def wrapper(*args, **kwargs):
        temp = time.clock()
        result = func(*args, **kwargs)
        time_res = time.clock() - temp
        print(func.__name__, time_res)
        return result
    return wrapper

# List of table fields which are to be processed to calculate correlation coeff
table_fields = ('device_id', # string, остальные integer
                'printed_pages_total',
                'errors_total',
                'unique_alerts_total',
                'unique_alerts_with_new_counter',
                'average_printed_pages_once',
                'deviation_printed_pages_once',
                # 'min_acquired_time',
                # 'max_acquired_time',
                # 'stdev_acquired_time',
                'counter_divided_by_acq_time_diff_hours',
                'temp',
                'temp_3')

# @benchmark
def pearsonr_numpy(x,y):
    """ Start Pearson Correlation Coef calculation through numpy module
        Work 4 times faster then handmade fucntion (sum/map/lambda...)!
    """
    return numpy.corrcoef(x,y)[0, 1]


def logger(func):
    """ Logger decorator
        Add logging to the file functionality
        Return normal sys.stdout stream
    """
    def wrapper(*args, **kwargs):
        temp = sys.stdout                   # Temp for stream recover
        sys.stdout = open('log.txt', 'a')   # Stdout to file
        print('========== Pearson corr coeff calculation results ==========')
        print(datetime.datetime.now())
        print('=== SQL query: ===')
        print(MYSQL_SELECT, '\n')
        func(*args, **kwargs)
        print('=============================================================')
        sys.stdout.close()                  # Push buffers to disk
        sys.stdout = temp
        print(open('log.txt').read())
    return wrapper


def insert_result(pair, pearson_coeff, mean1, stdev1, mean2, stdev2):
    """ Insert corr coeff calculation results to the result collection table
    """
    try:
        conn = MySQLdb.connect(
                            host="localhost",   # hostname
                            user="root",        # username
                            passwd="111111",    # password
                            db="analytics"      # data base name
                            )

        cur = conn.cursor()
        # created = now()
        cur.execute(
            """INSERT INTO stats_correlation (pair, coeff, mean1, stdev1, mean2, stdev2)
            VALUES (%s,%s,%s,%s,%s,%s)""",
            (pair, pearson_coeff, mean1, stdev1, mean2, stdev2)
        )
        conn.commit()

    except Error as err:
        print(err)
        conn.rollback()

    finally:
        try:
            if conn:
                conn.close()
        except:
            print('connection is absent')
#======================================================
@logger
def sql_readdata():
    """ Read data from MySQL table
        Start correlation coef calculation
    """
    try:
        conn = MySQLdb.connect(
                            host="localhost",   # hostname
                            user="root",        # username
                            passwd="111111",    # password
                            db="analytics"      # data base name
                            )
        cur = conn.cursor()

        processed = []
        for i in range(1, len(table_fields)):
            len_i = len(table_fields)
            for j in range(1, len(table_fields)):
                if (i, j) in processed: continue    # skip repeated pairs
                processed.append((j, i))
                len_j = len(table_fields)
                if i == j: continue         # skip comparison with itself
                f1 = table_fields[i]
                f2 = table_fields[j]

                cur.execute(MYSQL_SELECT.
                        format(field1=f1, field2=f2))

                list1 = []
                list2 = []
                for row in cur.fetchall():
                    list1.append(row[0])
                    list2.append(row[1])

                # Statistic parameters calculation
                pearson_coeff_numpy = pearsonr_numpy(list1, list2)

                mean1 = statistics.mean(list1)
                mean2 = statistics.mean(list2)
                stdev1 = statistics.stdev(list1)
                stdev2 = statistics.stdev(list2)


                # Insert result to the DB:
                pair = '{0}__{1}'.format(table_fields[i], table_fields[j])
                insert_result(pair, pearson_coeff_numpy, mean1, stdev1, mean2, stdev2)


                print('corr =', pair.ljust(70), pearson_coeff_numpy)
                print('mean1  =', mean1)
                print('stdev1 =', stdev1)
                print('mean2  =', mean2)
                print('stdev2 =', stdev2, '\n')

    except Error as err:
        print(err)

    finally:
        try:
            if conn:
                conn.close()
        except:
            print('connection is absent')



if __name__ == '__main__':
    """ Start correlation factor calculation script
        when using as a separate file application
    """
    sql_readdata()




